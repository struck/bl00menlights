import math
import random

from st3m.application import Application, ApplicationContext
from ctx import Context
from st3m.input import InputController, InputState

import leds

import st3m.run

def dist(p, q):
       return math.sqrt((p[0] - q[0]) ** 2 + (p[1] - q[1]) ** 2)

class Circle():
    def __init__(self, point, radius, hue):
        self.point = point
        self.radius = radius
        self.hue = hue

class LEDsApp(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.input = InputController()
        self.led = 0.0
        self.x = -119.0
        self.petals = [
            [50, 20],
            [70, 20],
            [75, 35],
            [80, 65],
            [65, 80],
            [50, 90],
            [35, 80],
            [20, 65],
            [25, 35],
            [30, 20]
        ]
        self.leds = [
            [58, 0], # 0
            [65, 8],
            [70, 17],
            [73, 27],
            [76, 36], # 4
            [85, 36],
            [95, 36],
            [105, 37],
            [116, 41], # 8
            [109, 50],
            [102, 58],
            [95, 64],
            [87, 69], # 12
            [90, 78],
            [93, 88],
            [94, 98],
            [94, 109], # 16
            [83, 106],
            [74, 102],
            [66, 96],
            [58, 90], # 20
            [50, 96],
            [42, 102],
            [33, 106],
            [22, 109], # 24
            [22, 98],
            [24, 88],
            [26, 78],
            [29, 69], # 28
            [22, 64],
            [13, 58],
            [6, 50],
            [0, 41], # 32
            [10, 37],
            [20, 36],
            [31, 36],
            [40, 36], # 36
            [43, 27],
            [46, 17],
            [51, 8],
        ]
        self.selected = -1
        self.radius = 0
        self.rot = 0.0
        leds.set_brightness(69)
        self.circles = []
        self.wait = 0
        self.speed = 10
        self.color = False
        self.brightness = 69

    def draw(self, ctx: Context) -> None:
        ctx.line_width = 4.0
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.rgb(0, 0.5, 0.5).rotate(self.rot).move_to(int(self.x)-2, -120).line_to(int(self.x)-2, 119).stroke()
        ctx.rgb(0, 0.7, 0.3).rotate(self.rot).move_to(int(self.x)-1, -120).line_to(int(self.x)-1, 119).stroke()
        ctx.rgb(0, 1, 0).rotate(self.rot).move_to(int(self.x), -120).line_to(int(self.x), 119).stroke()
        ctx.rgb(0, 0.7, 0.3).rotate(self.rot).move_to(int(self.x)+1, -120).line_to(int(self.x)+1, 119).stroke()
        ctx.rgb(0, 0.5, 0.5).rotate(self.rot).move_to(int(self.x)+2, -120).line_to(int(self.x)+2, 119).stroke()

        if len(self.circles) > 0:
            for i in range(40):
                hue = 0
                value = 0
                for c in self.circles:
                    d = abs(dist(self.leds[i], c.point) - c.radius)
                    value += 1/((d/4)+1)
                    if self.color:
                        hue += c.hue * value
                    else:
                        hue += c.hue

                leds.set_hsv(i, hue/len(self.circles) , 1, value/len(self.circles))

        leds.update()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.led += delta_ms / 45
        self.x += delta_ms / 20
        self.rot += 0.01
        if self.x >= 119:
            self.x = -119.0
        if self.led >= 40:
            self.led = 0

        for i in range(10):
            petal = self.input.captouch.petals[i].whole
            if petal.pressed:
                self.circles.append(Circle(self.petals[i], 0, random.randint(0, 359)))

        for i in range(len(self.circles)):
            if self.circles[i].radius > 200:
                self.circles.pop(i)
                break
            else:
                self.circles[i].radius += delta_ms / self.speed
        
        if len(self.circles) < 1:
            leds.set_all_hsv(0, 0, 0)
            self.wait += delta_ms
            if self.wait > 10:
                self.circles.append(Circle([random.randint(20, 80), random.randint(20, 80)], 0, random.randint(30, 359)))
                self.wait = 0
        
        if self.input.buttons.app.right.pressed:
            self.brightness += 20
            if self.brightness > 255:
                self.brightness = 255
            leds.set_brightness(self.brightness)
        if self.input.buttons.app.left.pressed:
            self.brightness -= 20
            if self.brightness < 0:
                self.brightness = 0
            leds.set_brightness(self.brightness)
        if self.input.buttons.app.middle.pressed:
            self.color = not self.color


if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(LEDsApp(ApplicationContext()))
